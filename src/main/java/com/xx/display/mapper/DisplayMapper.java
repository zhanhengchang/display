package com.xx.display.mapper;

import com.xx.display.bean.Uncompetitive;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import java.util.List;
import java.util.Map;

@Mapper
public interface DisplayMapper {
    @Select("select * from result1 order by cityName desc, beginAddr desc, endAddr desc")
    List<Uncompetitive> getUncompetitiveList();

    @Select("SELECT @rownum:=@rownum+1 AS rownum, A.* FROM(SELECT * FROM result2 " +
            "ORDER BY amount DESC, avg_price DESC)A, (SELECT @rownum:=0,@total:=null)B")
    List<Map<String, Object>> getRank();

    @Select("SELECT * FROM offline_result_1 ORDER BY cityName DESC")
    List<Map<String, Object>> getOfflineResult1();
}