package com.xx.display.controller;

import com.xx.display.bean.Uncompetitive;
import com.xx.display.service.DisplayService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/displayController")
public class DisplayController {
    @Resource
    DisplayService displayService;

    @RequestMapping("/getUncompetitiveList")
    @ResponseBody
    public List<Uncompetitive> getUncompetitiveList() {
        List<Uncompetitive> list = displayService.getUncompetitiveList();
        return list;
    }

    @RequestMapping("/getRank")
    @ResponseBody
    public List<Map<String, Object>> getRank() {
        List<Map<String, Object>> list = displayService.getRank();
        return list;
    }

    @RequestMapping("/getOfflineResult1")
    @ResponseBody
    public List<Map<String, Object>> getOfflineResult1() {
        List<Map<String, Object>> list = displayService.getOfflineResult1();
        return list;
    }
}
