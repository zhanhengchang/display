package com.xx.display.bean;

public class Uncompetitive {
    //这一批数据中“首汽约车”是最不具竞争力的厂商的产品信息，也即在这批数据中，相同起点、终点、相同时间段、相同车型，“首汽约车”价格最高
    //id, insert_time, cityname, beginaddr, endaddr, cartype, brandname, estimateprice, estimatedistance, estimatetimelength, usetime
    private int id;
    private String insert_time;
    private String cityname;
    private String beginaddr;
    private String endaddr;
    private String cartype;
    private String brandname;
    private double estimateprice;
    private double estimatedistance;
    private double estimatetimelength;
    private String usetime;

    public Uncompetitive() {

    }

    public Uncompetitive(int id, String insert_time, String cityname, String beginaddr, String endaddr, String cartype, String brandname, double estimateprice, double estimatedistance, double estimatetimelength, String usetime) {
        this.id = id;
        this.insert_time = insert_time;
        this.cityname = cityname;
        this.beginaddr = beginaddr;
        this.endaddr = endaddr;
        this.cartype = cartype;
        this.brandname = brandname;
        this.estimateprice = estimateprice;
        this.estimatedistance = estimatedistance;
        this.estimatetimelength = estimatetimelength;
        this.usetime = usetime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInsert_time() {
        return insert_time;
    }

    public void setInsert_time(String insert_time) {
        this.insert_time = insert_time;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getBeginaddr() {
        return beginaddr;
    }

    public void setBeginaddr(String beginaddr) {
        this.beginaddr = beginaddr;
    }

    public String getEndaddr() {
        return endaddr;
    }

    public void setEndaddr(String endaddr) {
        this.endaddr = endaddr;
    }

    public String getCartype() {
        return cartype;
    }

    public void setCartype(String cartype) {
        this.cartype = cartype;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public double getEstimateprice() {
        return estimateprice;
    }

    public void setEstimateprice(double estimateprice) {
        this.estimateprice = estimateprice;
    }

    public double getEstimatedistance() {
        return estimatedistance;
    }

    public void setEstimatedistance(double estimatedistance) {
        this.estimatedistance = estimatedistance;
    }

    public double getEstimatetimelength() {
        return estimatetimelength;
    }

    public void setEstimatetimelength(double estimatetimelength) {
        this.estimatetimelength = estimatetimelength;
    }

    public String getUsetime() {
        return usetime;
    }

    public void setUsetime(String usetime) {
        this.usetime = usetime;
    }

    @Override
    public String toString() {
        return          id +
                "\t" + insert_time +
                "\t" + cityname +
                "\t" + beginaddr +
                "\t" + endaddr +
                "\t" + cartype +
                "\t" + brandname +
                "\t" + estimateprice +
                "\t" + estimatedistance +
                "\t" + estimatetimelength +
                "\t" + usetime ;
    }
}
