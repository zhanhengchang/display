package com.xx.display.service;

import com.xx.display.bean.Uncompetitive;
import com.xx.display.mapper.DisplayMapper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class DisplayService {
    @Resource
    private DisplayMapper displayMapper;

    public List<Uncompetitive> getUncompetitiveList() {
        List<Uncompetitive> list = displayMapper.getUncompetitiveList();
        return list;
    }

    public List<Map<String, Object>> getRank(){
        List<Map<String, Object>> list = displayMapper.getRank();
        return list;
    }

    public List<Map<String, Object>> getOfflineResult1(){
        List<Map<String, Object>> list = displayMapper.getOfflineResult1();
        return list;
    }
}